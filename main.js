'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ColorCube = function () {
	function ColorCube() {
		var container = arguments.length <= 0 || arguments[0] === undefined ? '#webgl-container' : arguments[0];

		_classCallCheck(this, ColorCube);

		var self = this;

		self.defaults = {
			colors: [0xff0000, 0x00ff00, 0x0000ff, 0xF7005B, 0xffffff, 0x2AFFF0, 0xFFEF00, 0x5DFF79],
			scene: new THREE.Scene(),
			camera: new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.1, 1000),
			renderer: new THREE.WebGLRenderer(),
			webGLContainer: document.querySelector(container),
			raycaster: new THREE.Raycaster(),
			mouse: new THREE.Vector2()
		};
	}

	_createClass(ColorCube, [{
		key: 'init',
		value: function init() {
			var self = this;
			var _self$defaults = self.defaults;
			var scene = _self$defaults.scene;
			var camera = _self$defaults.camera;

			self.setContainer();
			var cube = self.createCubeWithEdges();
			self.addVertices(cube);
			scene.add(cube);
			self.render();
			self.setControls();
			document.addEventListener('mousedown', self.mainInteraction.bind(self));
		}
	}, {
		key: 'setContainer',
		value: function setContainer() {
			var self = this;
			var _self$defaults2 = self.defaults;
			var renderer = _self$defaults2.renderer;
			var webGLContainer = _self$defaults2.webGLContainer;

			renderer.setSize(window.innerWidth, window.innerHeight);
			webGLContainer.appendChild(renderer.domElement);
		}
	}, {
		key: 'setControls',
		value: function setControls() {
			var self = this;
			var _self$defaults3 = self.defaults;
			var renderer = _self$defaults3.renderer;
			var camera = _self$defaults3.camera;

			var controls = new THREE.OrbitControls(camera, renderer.domElement);
		}
	}, {
		key: 'createCubeWithEdges',
		value: function createCubeWithEdges() {
			var self = this;
			var cube = self.createCube();
			self.edges = self.setEdges(cube);
			cube.add(self.edges);
			return cube;
		}
	}, {
		key: 'createCube',
		value: function createCube() {
			var data = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

			var self = this;
			var camera = self.defaults.camera;

			var geometryWidth = data.geometryWidth || 20;
			var geometryHeight = data.geometryHeight || 20;
			var geometryDepth = data.geometryDepth || 20;

			var geometry = new THREE.BoxGeometry(geometryWidth, geometryHeight, geometryDepth);
			var material = new THREE.MeshBasicMaterial({ color: 0x000000, vertexColors: THREE.VertexColors, transparent: true, opacity: 0 });
			var cube = new THREE.Mesh(geometry, material);
			camera.position.z = 80;
			camera.position.x = 50;
			camera.position.y = 50;
			return cube;
		}
	}, {
		key: 'setEdges',
		value: function setEdges(cube) {
			var edges = new THREE.EdgesHelper(cube, 0x0000ff);
			edges.material.linewidth = 5;
			return edges;
		}
	}, {
		key: 'addVertices',
		value: function addVertices(cube) {
			var self = this;
			var _self$defaults4 = self.defaults;
			var scene = _self$defaults4.scene;
			var colors = _self$defaults4.colors;

			var vertices = cube.geometry.vertices;

			vertices.forEach(function (vertex, i) {
				var vertexMaterial = new THREE.MeshBasicMaterial({ color: colors[i] });
				var vertexSphere = new THREE.SphereGeometry(0.70);
				var vertexMesh = new THREE.Mesh(vertexSphere, vertexMaterial);
				vertexMesh.position.x = vertex.x;
				vertexMesh.position.y = vertex.y;
				vertexMesh.position.z = vertex.z;
				scene.add(vertexMesh);
			});
		}
	}, {
		key: 'mainInteraction',
		value: function mainInteraction(event) {
			var self = this;
			var _self$defaults5 = self.defaults;
			var mouse = _self$defaults5.mouse;
			var scene = _self$defaults5.scene;
			var raycaster = _self$defaults5.raycaster;
			var renderer = _self$defaults5.renderer;
			var camera = _self$defaults5.camera;

			mouse.x = event.clientX / renderer.domElement.width * 2 - 1;
			mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;
			self.setFromCamera(raycaster, mouse, camera);
			var intersects = raycaster.intersectObjects(scene.children, true);
			if (intersects.length && intersects[0].object.geometry.type === 'SphereGeometry') {
				var color = intersects[0].object.material.color;
				self.edges.material.color = color;
			}
		}
	}, {
		key: 'setFromCamera',
		value: function setFromCamera(raycaster, coords, origin) {
			raycaster.ray.origin.copy(origin.position);
			raycaster.ray.direction.set(coords.x, coords.y, 0.5).unproject(origin).sub(origin.position).normalize();
		}
	}, {
		key: 'render',
		value: function render() {
			var self = this;
			var _self$defaults6 = self.defaults;
			var renderer = _self$defaults6.renderer;
			var scene = _self$defaults6.scene;
			var camera = _self$defaults6.camera;

			requestAnimationFrame(self.render.bind(self));
			renderer.render(scene, camera);
		}
	}]);

	return ColorCube;
}();

var cube = new ColorCube();
cube.init();
