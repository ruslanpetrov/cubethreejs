class ColorCube {
	constructor(container = '#webgl-container') {
		const self = this;

		self.defaults = {
			colors: [0xff0000, 0x00ff00, 0x0000ff, 0xF7005B, 0xffffff, 0x2AFFF0, 0xFFEF00, 0x5DFF79],
			scene: new THREE.Scene(),
			camera: new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.1, 1000),
			renderer: new THREE.WebGLRenderer(),
			webGLContainer: document.querySelector(container),
			raycaster: new THREE.Raycaster(),
			mouse: new THREE.Vector2()
		};

	}

	init() {
		const self = this;
		const {scene, camera} = self.defaults;
		self.setContainer();
		const cube = self.createCubeWithEdges();
		self.addVertices(cube);
		scene.add(cube);
		self.render();
		self.setControls();
		document.addEventListener('mousedown', self.mainInteraction.bind(self));
	}

	setContainer() {
		const self = this;
		const {renderer, webGLContainer} = self.defaults;
		renderer.setSize(window.innerWidth, window.innerHeight);
		webGLContainer.appendChild(renderer.domElement);
	}

	setControls() {
		const self = this;
		const {renderer, camera} = self.defaults;
		const controls = new THREE.OrbitControls(camera, renderer.domElement);
	}

	createCubeWithEdges() {
		const self = this;
		const cube = self.createCube();
		self.edges = self.setEdges(cube);
		cube.add(self.edges);
		return cube;
	}

	createCube(data = {}) {
		const self = this;
		const {camera} = self.defaults;
		const geometryWidth = data.geometryWidth || 20;
		const geometryHeight = data.geometryHeight || 20;
		const geometryDepth = data.geometryDepth || 20;

		const geometry = new THREE.BoxGeometry(geometryWidth, geometryHeight, geometryDepth);
		const material = new THREE.MeshBasicMaterial({color: 0x000000, vertexColors: THREE.VertexColors, transparent: true, opacity: 0});
		const cube = new THREE.Mesh(geometry, material);
		camera.position.z = 80;
		camera.position.x = 50;
		camera.position.y = 50;
		return cube;
	}

	setEdges(cube) {
		const edges = new THREE.EdgesHelper(cube, 0x0000ff);
		edges.material.linewidth = 5;
		return edges;
	}

	addVertices(cube) {
		const self = this;
		const {scene, colors} = self.defaults;
		const vertices = cube.geometry.vertices;

		vertices.forEach((vertex, i) => {
			const vertexMaterial = new THREE.MeshBasicMaterial({color: colors[i]});
			const vertexSphere = new THREE.SphereGeometry(0.70);
			const vertexMesh = new THREE.Mesh(vertexSphere, vertexMaterial);
			vertexMesh.position.x = vertex.x;
			vertexMesh.position.y = vertex.y;
			vertexMesh.position.z = vertex.z;
			scene.add(vertexMesh);
		});
	}

	mainInteraction(event){
		const self = this;
		const {mouse, scene, raycaster, renderer, camera} = self.defaults;
		mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
		mouse.y = - (event.clientY / renderer.domElement.height) * 2 + 1;
		self.setFromCamera(raycaster, mouse, camera );
		const intersects = raycaster.intersectObjects( scene.children, true );
		if ( intersects.length && intersects[0].object.geometry.type === 'SphereGeometry') {
			const color = intersects[0].object.material.color;
			self.edges.material.color = color;
		}
	}

	setFromCamera(raycaster, coords, origin) {
		raycaster.ray.origin.copy(origin.position);
		raycaster.ray.direction.set(coords.x, coords.y, 0.5).unproject(origin).sub(origin.position).normalize();
	}

	render() {
		const self = this;
		const {renderer, scene, camera} = self.defaults
		requestAnimationFrame(self.render.bind(self));
		renderer.render(scene, camera);
	}
}

const cube = new ColorCube();
cube.init();